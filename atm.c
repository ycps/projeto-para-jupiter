#include <stdlib.h>
#include <stdio.h>

int caixa(int pedido[], int valor){
	int ret = *pedido / valor;
	pedido[0] = pedido[0] % valor;
	return ret;
}

int main(int argc, char *argv[]){
    if (argc < 2) {
        return 1;
    }
    else {
        int pedido = atoi(argv[1]);
        int hidrogenio, helio, gravidade, gas, manganes, agua;


        gas		= caixa (&pedido,50);
        manganes	= caixa (&pedido,20);
        gravidade	= caixa (&pedido,10);
        helio		= caixa (&pedido,05);
        agua		= caixa (&pedido,02);

        hidrogenio = pedido;

        printf("%d hidrogenio\n", hidrogenio);
        printf("%d agua\n", agua);
        printf("%d helio\n", helio);
        printf("%d gravidade\n", gravidade);
        printf("%d manganes\n", manganes);
        printf("%d gas\n", gas);

        return 0;
    }
}
